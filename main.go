package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/exu/image-gallery/gallery"
)

func uploadFile(w http.ResponseWriter, r *http.Request) {
	u := gallery.NewImageUploader()
	file, err := u.Upload(r)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("uploading error: %v", err)))
		return
	}

	res := gallery.NewResizer()
	paths, err := res.Resize(file, "images/budowa")
	w.Write([]byte(fmt.Sprintf("Response: %v, paths: %v", err, paths)))
}

func setupRoutes() {
	http.HandleFunc("/upload", uploadFile)
	http.ListenAndServe(":8080", nil)
}

func main() {
	setupRoutes()
}
