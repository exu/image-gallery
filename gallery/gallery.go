package gallery

import "fmt"

type Size struct {
	W int
	H int
}

func (s Size) String() string {
	return fmt.Sprintf("%dx%d", s.W, s.H)
}

type Gallery struct {
	Header Header
	Images []Image
}

type Image struct {
	UrlBig      string
	UrlSmall    string
	Description string
	Size        Size
}

type Header struct {
	CSSClass    string
	Title       string
	Description string
	Color       string
}

func NewIndexedImage(group string, idx int) Image {
	return Image{
		fmt.Sprintf("/assets/img/gallery/%s/%s_%d_big.jpg", group, group, idx),
		fmt.Sprintf("/assets/img/gallery/%s/%s_%d_small.jpg", group, group, idx),
		"",
		Size{1024, 768},
	}
}
