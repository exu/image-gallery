package gallery

import (
	"fmt"
	"image/jpeg"
	"io"
	"os"

	"github.com/nfnt/resize"
)

type Resizer interface {
	Resize()
}

func NewResizer() *JpgResizer {
	return &JpgResizer{
		Sizes:           []Size{Size{320, 240}, Size{1024, 768}},
		FileNamePattern: "%s-%d.jpg",
	}
}

type JpgResizer struct {
	Sizes           []Size
	FileNamePattern string
}

func (r *JpgResizer) Resize(file io.ReadCloser, filePrefix string) (paths []string, err error) {
	defer file.Close()

	// decode jpeg into image.Image
	img, err := jpeg.Decode(file)
	if err != nil {
		fmt.Printf("jpeg.Decode ERROR: %+v\n", err)
		return paths, fmt.Errorf("jpeg.Decode error: ", err)
	}

	for _, size := range r.Sizes {
		m := resize.Resize(uint(size.W), 0, img, resize.Lanczos3)

		filePath := fmt.Sprintf(r.FileNamePattern, filePrefix, size.W)
		out, err := os.Create(filePath)
		if err != nil {
			return paths, fmt.Errorf("os.Create error (path:%s), %v", filePath, err)
		}
		defer out.Close()

		// write new image to file
		err = jpeg.Encode(out, m, nil)
		if err != nil {
			return paths, fmt.Errorf("jpeg.Encode error, %v", err)
		}

		paths = append(paths, filePath)
	}

	return
}
