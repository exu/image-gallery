package gallery

import (
	"fmt"
	"io"
	"net/http"
)

// NewImageUploader reutrns new image uploader
func NewImageUploader() *ImageUploader {
	return &ImageUploader{
		DestinationDirectory: "images",
		FormFileName:         "image",
		ValidTypes:           []string{"image/jpeg", "image/jpg"},
	}
}

// ImageUploader uploads images
type ImageUploader struct {
	DestinationDirectory string
	FormFileName         string
	ValidTypes           []string
}

func (u *ImageUploader) validate(contentType string) error {
	for _, validContentType := range u.ValidTypes {
		if contentType == validContentType {
			return nil
		}
	}

	return fmt.Errorf("Proszę przekazać obrazki w jendym a z formatów: %v JPG (otrzymałem: %v)", u.ValidTypes, contentType)
}

// Upload reads uploaded file
func (u *ImageUploader) Upload(r *http.Request) (io.ReadCloser, error) {
	// Parse our multipart form, 10 << 20 specifies a maximum upload of 10 MB files.
	r.ParseMultipartForm(50 << 20)

	file, handler, err := r.FormFile(u.FormFileName)
	if err != nil {
		return nil, err
	}

	contentType := handler.Header.Get("content-type")
	if err := u.validate(contentType); err != nil {
		return nil, err
	}

	return file, err
}
